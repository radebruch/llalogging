# LLALogging

A logging package.

## Getting Started

```swift
// import the library
import LLALogging

// create a logger
let log = LogManager.shared.createLogger()

// and go ahead: log stuff!
log.debug("Here we go!")
```

## Log Levels
The log level for log output is set as follows: `LogManager.shared.logLevel = .verbose`. It applies to all loggers created by that manager instance (i.e., the instance being a singleton, it applies to all loggers!). The level can be changed at any time by assigning a new value. The change takes effect immediately. Only messages of the selected level or a higher level will be logged. _**The default log level is `.debug`. It will be in use if the client application doesn't explicitly set a different one!**_

Available log levels are (increasing order): `.verbose`, `.debug`, `.info`, `.warn`, `.error`

Messages are logged using the function named after the log level.

## Asynchronous Logging
Sending log messages is an asynchronous task. If a log statement is being executed, the log data will be collected into a log event and that event will then be pushed onto a serial background queue: the **LoggerQueue**. This is a FIFO queue, i.e. the events will be processed strictly in the same temporal order as they were received.

Asynchronous processing allows the program flow to continue immediately, without having to wait for possible overhead computation that may arise from rendering the log messages and/or writing them to the log output.

*Note that due to the logging being asynchronous, log messages which are still on the queue (not yet rendered / written) when the app terminates will be lost. Call `LogManager.shared.sync()` to wait for all pending messages to be written*

## Custom Log Renderer
The module uses a log renderer to convert log events into strings that are then written to the log output. A default renderer comes with the library, but client applications can provide their own implementation if the format created by the default renderer doesn't suit their needs:

```swift
let myRenderer: LogRendererProtocol = createCustomRenderer()
LogManager.shared.renderer = myRenderer
```

The LogRendererProtocol is defined as follows:
```swift
public protocol LogRendererProtocol {
    func render(logData: LogData, for level: LogLevel) -> [String]
}
```
The `render` function will get a `LogData` structure and the log level currently in use. Its job is it, to create one or more output lines from the `LogData`. It returns these lines to the caller.

The `LogData` struct contains all data gathered when the log function was called. It contains the source file name, line number, column number, a timestamp, the log level of the event, etc. Also. of course, it contains the payload, the log message itself.

The log level currently in use is handed in so the renderer can decide to render information differently depending on the current level. The default renderer for example usually renders `Data` objects by just formatting the approximate byte size, like `[3 kB of Data]`. However, if the log level is `.verbose`, it would render the complete content of the `Data`.

Look at the implementation of `DefaultLogRenderer` for more details.

The renderer will be called on the background **LoggerQueue** when the corresponding log event is being processed. A custom renderer will be used by the framework immediately after it has been assigned.

## Custom Log Writer
The module uses a log writer to write pre-rendered log messages to the output. A default writer comes with the module (which writes everything to the console using Swift's `print(...)` function) but the client applications can provide their own writer implementation if they whish to write the log output to a some other destination:

```swift
let myWriter: LogWriterProtocol = createCustomWriter()
LogManager.shared.writer = myWriter
```
Where the `LogWriterProtocol` is defined as follows:
```swift
public protocol LogWriterProtocol {
    func writeLogLines(_ lines: [String])
}
```

The writer will be called on the background **LoggerQueue** when the corresponding renderer has finished formatting the message. A custom writer will be used by the framework imediately after it has been assigned.
