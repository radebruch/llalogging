# Changelog

## 1.0.0 (2019-10-19)
Finalized the version. Minor changes and enhancements. Finish documentation.

## 0.9.0 (2019-10-18)
Initial release, basically for internal integration.
