// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

// The functions have a lot of default parameters here, which are required to
// grab the log context / location automatically. A user using the log functions
// will never have to use them so it's not a big deal:
// ==> disable linter warning:
//
// swiftlint:disable function_parameter_count

import Foundation

/// Together with `LogManager` this class, `Logger` makes up the core of the `LLALogging` package.
///
/// Instance of `Logger` can be created using `LogManager.shared.createLogger()`. Direct
/// instantiation is not possible.
///
/// Logger provides triplets of overloaded functions: one to log a `String`, one to log a `Data` instance, and one to log
/// a call stack trace..
/// For each log level, there's one such triplet, named after the log level:
///  ```swift
/// func verbose(_ text: String)
/// func verbose(_ data: Data)
/// func verbose(_ thread: Thread.Type)
///
/// func debug(_ text: String)
/// func debug(_ data: Data)
/// func debug(_ thread: Thread.Type)
///
/// func info(_ text: String)
/// func info(_ data: Data)
/// func info(_ thread: Thread.Type)
///
/// func warn(_ text: String)
/// func warn(_ data: Data)
/// func warn(_ thread: Thread.Type)
///
/// func error(_ text: String)
/// func error(_ data: Data)
/// func error(_ thread: Thread.Type)
/// ```
/// *Actually, the functions are a little more complex, as they have a lot more parameters - but none of these parameters
/// when logging. For all additional parameters, default values are supplied that are used to grab context information about
/// the file name, line number, etc.*
public final class Logger {
    /// Context information. The logger will include this context string in each `LogData` instance it creates. Later, the
    /// context string may be used by the renderer.
    public var context: String?

    private weak var manager: LogManager?

    init(manager: LogManager) {
        self.manager = manager
    }

    /// Wait for all pending log messages to be rendered and written.
    ///
    /// This function forwards the call to `LogManager`'s `sync()` function and is here for convenience.
    public func sync() {
        manager?.sync()
    }

    // MARK: - verbose -

    public func verbose(_ message: String, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.text(message), level: .verbose, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func verbose(_ data: Data, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.data(data), level: .verbose, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func verbose(_ thread: Thread.Type, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.callstack(thread.callStackSymbols), level: .verbose, file: file, function: function, line: line, column: column, dso: dso)
    }

    // MARK: - debug -

    public func debug(_ message: String, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.text(message), level: .debug, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func debug(_ data: Data, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.data(data), level: .debug, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func debug(_ thread: Thread.Type, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.callstack(thread.callStackSymbols), level: .debug, file: file, function: function, line: line, column: column, dso: dso)
    }

    // MARK: - info -

    public func info(_ message: String, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.text(message), level: .info, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func info(_ data: Data, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.data(data), level: .info, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func info(_ thread: Thread.Type, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.callstack(thread.callStackSymbols), level: .info, file: file, function: function, line: line, column: column, dso: dso)
    }

    // MARK: - warn -

    public func warn(_ message: String, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.text(message), level: .warn, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func warn(_ data: Data, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.data(data), level: .warn, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func warn(_ thread: Thread.Type, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.callstack(thread.callStackSymbols), level: .warn, file: file, function: function, line: line, column: column, dso: dso)
    }

    // MARK: - error -

    public func error(_ message: String, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.text(message), level: .error, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func error(_ data: Data, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.data(data), level: .error, file: file, function: function, line: line, column: column, dso: dso)
    }

    public func error(_ thread: Thread.Type, file: String = #file, function: String = #function, line: UInt = #line, column: UInt = #column, dso: UnsafeRawPointer = #dsohandle) {
        composeAndSend(.callstack(thread.callStackSymbols), level: .error, file: file, function: function, line: line, column: column, dso: dso)
    }

    // MARK: - where the work is done -

    private func composeAndSend(_ payload: LogPayload, level: LogLevel, file: String, function: String, line: UInt, column: UInt, dso: UnsafeRawPointer) {
        let data = LogData(
            logLevel: level,
            logContext: context,
            timeStamp: Date(),
            fileName: file,
            funcName: function,
            lineNumber: line,
            columnNumber: column,
            dsoHandle: dso,
            payload: payload)
        manager?.sendLog(data)
    }
}
