// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

class DefaultLogRenderer: LogRendererProtocol {
    private static let timestampFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyMMdd'/'HHmmss.SSS"
        return formatter
    }()

    func render(logData: LogData, for logLevel: LogLevel) -> [String] {
        assert(logData.logLevel >= logLevel, "Not supposed to log due to level setting.")

        let prefix = "✪"
        let level = renderLevel(logData)
        let timestamp = renderTimestamp(logData)
        let location = renderLocation(logData)
        let message = renderMessage(logData, for: logLevel)

        if message.count == 1 {
            return ["\(prefix) \(level): \(timestamp): \(location) \(message[0])"]
        } else {
            var lines = message.map { "\(prefix) \(level): > \($0)" }
            lines.insert("\(prefix) \(level): \(timestamp): \(location) (\(message.count) lines):", at: 0)
            return lines
        }
    }

    // MARK: Detail Renderers

    private func renderLevel(_ data: LogData) -> String {
        switch data.logLevel {
        case .verbose:
            return "V"
        case .debug:
            return "D"
        case .info:
            return "I"
        case .warn:
            return "W"
        case .error:
            return "E"
        }
    }

    private func renderTimestamp(_ data: LogData) -> String {
        return DefaultLogRenderer.timestampFormatter.string(from: data.timeStamp)
    }

    private func renderLocation(_ data: LogData) -> String {
        let fileName = data.fileName.split(separator: "/").last ?? ""
        let context = data.logContext ?? data.libraryName
        return "[\(context)] \(fileName):\(data.lineNumber) - \(data.funcName) -"
    }

    private func renderMessage(_ data: LogData, for level: LogLevel) -> [String] {
        switch data.payload {
        case let .text(msg):
            return msg.split(separator: "\n").map { String($0) }
        case let .callstack(stacktrace):
            var lines = stacktrace
            lines.insert("[Call Stack Trace]:", at: 0)
            return lines
        case let .data(data):
            if level == .verbose {
                return renderDataVerbose(data)
            }
            let fmt = ByteCountFormatter()
            fmt.countStyle = .binary
            return ["[\(fmt.string(fromByteCount: Int64(data.count))) of Data]"]
        }
    }

    private func renderDataVerbose(_ data: Data) -> [String] {
        var lines = ["[Verbose content of Data (\(data.count) Bytes)]:"]
        var index = data.startIndex
        while data.endIndex - index > 0 {
            let address = data.count < 65535 ? String(format: "%04X", index) : ""
            let sliceEnd = data.index(index, offsetBy: 32, limitedBy: data.endIndex) ?? data.endIndex
            let slice = data.subdata(in: index ..< sliceEnd)
            let hexBytes = slice.reduce(("", 1)) { tupel, byte in
                var str = tupel.0 + String(format: "%02X ", byte)
                if tupel.1 == 8 {
                    str += " "
                } else if tupel.1 == 16 {
                    str += "  "
                } else if tupel.1 == 24 {
                    str += " "
                }
                return (str, tupel.1 + 1)
            }.0

            let filler = String([Character](repeating: " ", count: max(100 - hexBytes.count, 0)))
            let text = slice.reduce("") {
                let c = Character(Unicode.Scalar($1))
                var str = $0
                if c.isASCII, c.isLetter || c.isHexDigit || c.isPunctuation || c == " " {
                    str += String(c)
                } else {
                    str += "▫︎"
                }
                return str
            }
            lines.append("\(address)| \(hexBytes)\(filler)| \(text)")
            index = sliceEnd
        }
        return lines
    }
}
