// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// All information gathered when a log function is being called by the client application is gathered
/// in the `LogData` struct.
///
/// The message (or, more generally, the information) to be logged is stored in the `payload`. But
/// there are a lot of other information collected under the hood when the log function is called.
/// These are (among others) the file name, line number, the name of the function and a timestamp.
public struct LogData {
    /// The log level for this log data. The data will only be logged if it is at least equal to the current
    /// log level set in the `LogManager`. Otherwise it will be discarded.
    public let logLevel: LogLevel
    /// A context string that can be used to transfer information to the renderer. The `DefaultLogRenderer`
    /// will render the context string in square brackets in front of the file name in the log line if
    /// it is non-`nil`. (If it's nil it will render the `libraryName` instead).
    public let logContext: String?
    /// The timestamp marking the point in time when the log function was called.
    public let timeStamp: Date
    /// The file name (path string) of the source file where the log function was called.
    public let fileName: String
    /// The name of the function where the loc function was called.
    public let funcName: String
    /// The line number in the source file where the log function was called.
    public let lineNumber: UInt
    /// The column number where the first argument of the log function is located within the line .
    public let columnNumber: UInt
    /// A pointer to the `dl_info` struct of the *Dynamic Shared Object* where the log function
    /// was called. (I.e. library name or the bundle name of the executable).
    public let dsoHandle: UnsafeRawPointer
    /// The log payload. This is encapsulates the log message the client application wants to log.
    public let payload: LogPayload

    /// The name of the library or the executable. This string will be extracted dynamically from the
    /// `dsoHandle`.
    public var libraryName: String {
        var dlinfo: dl_info = dl_info()
        dladdr(dsoHandle, &dlinfo)
        let path = String(cString: dlinfo.dli_fname)
        let libName = URL(fileURLWithPath: path).lastPathComponent
        return libName
    }
}
