// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// The log level controls if a specific instance of `LogData` will be rendered and written to the output or not.
///
/// Each instance of `LogData` has an assigned log level. It categorises the `LogData` with respect to its
/// type of content and to what degree the content may be relevant for a person reading the log output .
///
/// Also, the `LogManager` has a property named `logLevel` that specifies a level of verbosity for the log
/// output.
///
/// In other words, the `logLevel` associated with `LogData` describes what the content is: Is it relevant
/// for debugging, is it a general information. Or is it even a warning or information about a serious error.
///
/// In contrast, the `LogManager`'s `logLevel` is a filter setting, allowing to suppress information considered
/// irrelevant and restricting the log output to only messages at or above a limit level.
public enum LogLevel: Int, Comparable {
    /// Intended for _very_ detailed output for debugging.
    ///
    /// It is important to keep in mind that by default, the `LogManager`'s `logLevel` will be set to
    /// `.debug`. As a consequence, messages with this level will not be rendered unless the client application
    /// explicitly sets `LogManager.shared.logLevel = .verbose`
    case verbose = 0
    /// Log output relevant during development and for general debugging.
    case debug = 1
    /// Information that may help understanding the application state during normal operation.
    case info = 2
    /// Messages that are important to understand unusual operation states that may need attention by operating
    /// personnel.
    case warn = 3
    /// Errors. Use this level only if the program encounters an actual error that prevents the program from
    /// continuing its normal control flow.
    case error = 4

    public static func < (lhs: LogLevel, rhs: LogLevel) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
