// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// Log renderers must conform to this protocol.
///
/// Clients can provide their own implementation of a log renderer. A custom renderer can be activated by
/// assigning it to `LogManager.shared.renderer`.
public protocol LogRendererProtocol {
    /// Render the content of the `logData` into one or more lines of text (each represented by a string in
    /// the returned string array) that can then subsequently be handed over to the log writer.
    /// - Parameter logData: the log data to be rendered
    /// - Parameter level: the current log level for output. Renderers can decide to render content
    /// differently depending on the level (e.g. deliver a more concise representation in level `.info` and
    /// a more detailed one in level `.verbose`.
    func render(logData: LogData, for level: LogLevel) -> [String]
}
