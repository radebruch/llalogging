// MIT License
//
// Copyright © 2018-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// Alongside `Logger`, this is one central classes in the `LLALogging` package. Acting as a singleton
/// factory, it allows the client application to create instances of `Logger`.
///
/// In order to start logging, the application would have to do something similar to the following:
/// ```swift
/// import LLALogging
///
/// private let log = LogManager.shared.createLogger()
///
/// // then later:
/// log.debug("This is a log message")
/// ```
/// Also, the log manager maintains the serial queue where all log events are scheduled. The `sync()`
/// function may be called to wait for all pending log events to be rendered and output (e.g. before
/// terminating the app).
///
/// The manager allows customising the way log messages are rendered (this can be done by setting a
/// custom `renderer`). Moreover, the destination for the log output can be customised by setting a
/// custom `writer`.
///
/// Finally, the log level for the log output can be defined by assigning to the `logLevel` property.
public final class LogManager {
    /// The singleton instance of this class.
    public static let shared: LogManager = LogManager()

    /// The log level for log output. Only messages that were sent to the `Logger` with at least this
    /// level will be processed. Messages with a lower level will be dropped.
    public var logLevel: LogLevel
    /// The log renderer. Its job is it to format the `LogData` into one or more lines of text so they can
    /// be read by the intended addressee of the log output (a human or an automatic monitoring system).
    /// A default renderer is provided by the `LLALogging` package.
    public var renderer: LogRendererProtocol
    /// The log writer. It is used to write the lines (created by the `renderer`) to the log output.
    /// A default writer is provided by the `LLALogging` package.
    public var writer: LogWriterProtocol

    private let logQ = DispatchQueue(label: "lla.LogQ", qos: .background)

    init() {
        logLevel = .debug
        renderer = DefaultLogRenderer()
        writer = DefaultLogWriter()
    }

    /// Create a new instance of a logger.
    public func createLogger() -> Logger {
        return Logger(manager: self)
    }

    /// Create a new instance of a logger and set the given context string.
    /// - Parameter context: The context string. It will be included in all `LogData` created by this
    /// and can be used in custom renderers to create context sensitive behaviour.
    public func createLogger(for context: String) -> Logger {
        let logger = createLogger()
        logger.context = context
        return logger
    }

    /// Wait for all pending log messages to be rendered and written.
    ///
    /// This function will block until all log messages submitted to the logging system
    /// prior to the call will be rendered and written to the output. Only then, the
    /// function will return.
    public func sync() {
        logQ.sync {}
    }

    func sendLog(_ logData: LogData) {
        guard logData.logLevel >= logLevel else { return }
        logQ.async {
            let lines = self.renderer.render(logData: logData, for: self.logLevel)
            self.writer.writeLogLines(lines)
        }
    }
}
